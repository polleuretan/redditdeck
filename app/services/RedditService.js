var Promise = require('promise'),
  JSONP = require('./../lib/JSONP');

var REDDIT_JSONP_CALLBACK_VALUE = 'jsonp';

var RedditService = {};

/*
 * @method fetchNewPosts
 * @param {string} threadId
 * @returns {Promise}
 */
RedditService.fetchNewPosts = function(threadId){
  url = constructThreadNewPostsUrl(threadId);
  return new Promise(function (resolve, reject) {
    JSONP.get(url, {},
      function(data){
       resolve(data);
      },
      REDDIT_JSONP_CALLBACK_VALUE,
      function(error){
        reject(error);
      }
    );
  });
};

/*
 * @method constructThreadNewPostsUrl
 * @param {string} threadId
 * @returns {string}
 */
constructThreadNewPostsUrl = function(threadId){
  return 'http://www.reddit.com/r/' + threadId + '/new/.json';
};

module.exports = RedditService;


