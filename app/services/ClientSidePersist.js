var ClientSidePersist = {};

/*
 * @method setData
 * @param {string} key
 * @param {mixed} data
 */
ClientSidePersist.setData = function(key, data){
  window.localStorage.setItem(key, JSON.stringify(data));
};

/*
 * @method getData
 * @param {string} key
 * @returns {mixed}
 */
ClientSidePersist.getData = function(key){
  return JSON.parse(window.localStorage.getItem(key));
};

module.exports = ClientSidePersist;


