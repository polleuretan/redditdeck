var TimeDuration = {};

var SECONDS_IN_MINUTE = 60;
var MINUTES_IN_HOUR = 60;
var HOURS_IN_DAY = 24;

/*
 * @method format
 * @param {int} seconds
 * @returns {string}
 */
TimeDuration.format = function(seconds){
  var result = '', minutes, hours;
  if (seconds < SECONDS_IN_MINUTE) {
    result = 'less than 1m';
  } else if(seconds < SECONDS_IN_MINUTE * MINUTES_IN_HOUR) {
    minutes = Math.floor(seconds / SECONDS_IN_MINUTE);
    result = minutes + 'm';
  } else if(seconds < SECONDS_IN_MINUTE * MINUTES_IN_HOUR * HOURS_IN_DAY) {
    hours = Math.floor((seconds / SECONDS_IN_MINUTE) / MINUTES_IN_HOUR);
    result = hours + 'h';
  } else {
    days = Math.floor(
      ((seconds / SECONDS_IN_MINUTE) /MINUTES_IN_HOUR) / HOURS_IN_DAY);
    result = days + 'd';
  }

  return result;
};

module.exports = TimeDuration;
