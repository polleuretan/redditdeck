var EventEmitter = require('events').EventEmitter,
  Immutable = require('immutable'),
  dispatcher = require('./dispatcher'),
  constants = require('./constants'),
  ThreadModel = require('./models/Thread');

var POSTS_LIMIT = 25;

/*
 * @constructs Store
 * @param {Object} initialData
 */
var Store = function(initialData){
  if (initialData) {
    this._data = Immutable.fromJS(initialData);
  } else {
   this._data = Immutable.Map();
  }

  dispatcher.register(function(payload){
    switch(payload._type) {
      case constants.Threads.CREATE_ACTION:
        ThreadModel.rememberThread(payload.threadId);
        this.addThread(payload);
        this.emit(constants.Events.CHANGE);
      break;
      case constants.Threads.CLOSE_ACTION:
        ThreadModel.forgetThread(payload.threadId);
        this.deleteThread(payload);
        this.emit(constants.Events.CHANGE);
      break;
      case constants.Posts.NEW_POSTS_ACTION:
        this.appendPosts(payload);
        this.emit(constants.Events.CHANGE);
      break;
      case constants.Posts.NEW_POSTS_FAIL_ACTION:
        this.addThreadError(payload);
        this.emit(constants.Events.CHANGE);
      break;
      case constants.Posts.VIEWED_ACTION:
        this.markPostAsViewed(payload);
        this.emit(constants.Events.CHANGE);
      break;
      case constants.Posts.ALL_VIEWED_ACTION:
        this.markAllPostsAsViewed(payload);
        this.emit(constants.Events.CHANGE);
      break;
    }
  }.bind(this));
};

Store.prototype = Object.create(EventEmitter.prototype);

/*
 * @method addThread
 * @param {object} payload
 */
Store.prototype.addThread = function(payload){
  var threads = this.getThreads();
  threads = threads.push(Immutable.fromJS({id: payload.threadId}));

  this._data = this._data.set('threads', threads);
};

/*
 * @method deleteThread
 * @param {object} payload
 */
Store.prototype.deleteThread = function(payload){
  var threadEntry = getThreadEntry.call(this, payload.threadId),
    threadKey = threadEntry[0];

  this._data = this._data.deleteIn(['threads', threadKey]);
};

/*
 * @method appendPosts
 * @param {object} payload
 */
Store.prototype.appendPosts = function(payload){
  var threadId = payload.threadId,
    posts = this.getPosts(threadId),
    newPosts = payload.posts.map(setPostNewness.bind(this, true));

  posts = newPosts.concat(posts).take(POSTS_LIMIT);

  this.setPosts(threadId, posts);
};

/*
 * @method addThreadError
 * @param {object} payload
 */
Store.prototype.addThreadError = function(payload){
  var threadId = payload.threadId,
    thread = this.getThread(threadId);

  thread = thread.set('error', 'Error loading posts');

  this.setThread(threadId, thread);
};

/*
 * @method markPostAsViewed
 * @param {object} payload
 */
Store.prototype.markPostAsViewed = function(payload){
  var post = this.getPost(payload.threadId, payload.postId);
  post = setPostNewness(false, post);

  this.setPost(payload.threadId, payload.postId, post);
};

/*
 * @method markAllPostsAsViewed
 * @param {object} payload
 */
Store.prototype.markAllPostsAsViewed = function(payload){
  var posts = this.getPosts(payload.threadId);
  posts = posts.map(setPostNewness.bind(this, false));

  this.setPosts(payload.threadId, posts);
};

/*
 * @method getThreads
 * @returns {Immutable.List}
 */
Store.prototype.getThreads = function(){
  return this._data.get('threads', Immutable.List());
};

/*
 * @method getThread
 * @param {string} threadId
 * @returns {Immutable.Map}
 */
Store.prototype.getThread = function(threadId){
  var threadEntry = getThreadEntry.call(this, threadId),
    thread = threadEntry[1];

  return thread;
};

/*
 * @method setThread
 * @param {string} threadId
 * @param {Immutable.Map} thread
 */
Store.prototype.setThread = function(threadId, thread){
  var threadEntry = getThreadEntry.call(this, threadId),
    threadKey = threadEntry[0];

  this._data = this._data.setIn(['threads', threadKey], thread);
};

/*
 * @method getPost
 * @param {string} threadId
 * @param {string} postId
 */
Store.prototype.getPost = function(threadId, postId){
  var postEntry = getPostEntry.call(this, threadId, postId),
    post = postEntry[1];

  return post;
};

/*
 * @method setPost
 * @param {string} postId
 * @param {Immutable.Map} post
 */
Store.prototype.setPost = function(threadId, postId, post){
  var threadEntry = getThreadEntry.call(this, threadId),
    thread = threadEntry[1],
    postEntry = getPostEntry.call(this, threadId, postId),
    postIdx = postEntry[0];

  thread = thread.setIn(['posts', postIdx], post);

  this.setThread(threadId, thread);
};

/*
 * @method getPosts
 * @param {string} threadId
 * @returns {Immutable.List}
 */
Store.prototype.getPosts = function(threadId){
  var thread = this.getThread(threadId);
  return thread.get('posts', Immutable.List());
};

/*
 * @method setPosts
 * @param {string} threadId
 * @param {Immutable.List} posts
 */
Store.prototype.setPosts = function(threadId, posts){
  var thread = this.getThread(threadId);

  thread = thread.set('posts', posts);
  this.setThread(threadId, thread);
};

/*
 * setPostNewness
 * @param {boolean} isNew
 * @param {Immutable.List} post
 * @returns {Immutable.List}
 */
function setPostNewness(isNew, post){
  return post.set(constants.Threads.IS_NEW_ATTRIBUTE, isNew);
}

/*
 * getThreadEntry
 * @param {string} threadId
 * @returns {Immutable.Map}
 */
function getThreadEntry(threadId){
  var threads = this.getThreads();
  return getEntry(threads, threadId);
}

/*
 * getPostEntry
 * @param {string} threadId
 * @param {string} postId
 * @returns {Immutable.Map}
 */
function getPostEntry(threadId, postId){
  var posts = this.getPosts(threadId);
  return getEntry(posts, postId);
}

/*
 * getEntry
 * @param {Immutable.List} entries
 * @param {string} entryId
 * @throws {Error} if entry is not found
 * @returns {Immutable.Map}
 */
function getEntry(entries, entryId){
  entry = entries.findEntry(function(entry){
    return entry.get('id') === entryId;
  });
  if (!entry) throw new Error('Trying to get unknown entry ' + entryId);

  return entry;
}

module.exports = Store;
