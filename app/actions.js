var constants = require('./constants'),
  dispatcher = require('./dispatcher');

actions = {};
/*
 * addThread
 * @param {string} threadId
 */
actions.addThread = function(threadId){
  dispatcher.dispatch({
    _type: constants.Threads.CREATE_ACTION,
    threadId: threadId
  });
};

/*
 * newPosts
 * @param {Immutable.List} posts
 */
actions.newPosts = function(threadId, posts){
  dispatcher.dispatch({
    _type: constants.Posts.NEW_POSTS_ACTION,
    threadId: threadId,
    posts: posts,
  });
};

/*
 * newPostsFail
 * @param {Immutable.List} posts
 */
actions.newPostsFail = function(threadId){
  dispatcher.dispatch({
    _type: constants.Posts.NEW_POSTS_FAIL_ACTION,
    threadId: threadId
  });
};

/*
 * postViewed
 * @param {Immutable.List} posts
 */
actions.postViewed = function(threadId, postId){
  dispatcher.dispatch({
    _type: constants.Posts.VIEWED_ACTION,
    threadId: threadId,
    postId: postId
  });
};

/*
 * allPostsViewed
 * @param {Immutable.List} posts
 */
actions.allPostsViewed = function(threadId){
  dispatcher.dispatch({
    _type: constants.Posts.ALL_VIEWED_ACTION,
    threadId: threadId
  });
};

/*
 * closeThread
 * @param {string} threadId
 */
actions.closeThread = function(threadId){
  dispatcher.dispatch({
    _type: constants.Threads.CLOSE_ACTION,
    threadId: threadId,
  });
};

module.exports = actions;
