var Immutable = require('immutable'),
  RedditService = require('./../services/RedditService'),
  ClientSidePersist = require('./../services/ClientSidePersist');

/*
 * @constructs Thread
 * @param {string} threadId
 */
var Thread = function(threadId){
  this._threadId = threadId;
};

/*
 * @method rememberThread
 * @param {string} threadId
 */
Thread.rememberThread = function(threadId){
  var threads = ClientSidePersist.getData('threads');
  if (!threads) threads = [];

  threads.push(threadId);

  ClientSidePersist.setData('threads', threads);
};

/*
 * @method forgetThread
 * @param {string} threadId
 */
Thread.forgetThread = function(threadId){
  var threads = ClientSidePersist.getData('threads');
  if (!threads) return;
  var threadIdx = threads.indexOf(threadId);
  if (threadIdx !== -1) {
    threads.splice(threadIdx, 1);
  }

  ClientSidePersist.setData('threads', threads);
};

/*
 * @method getRememberedThreads
 */
Thread.getRememberedThreads = function(){
  var threads = ClientSidePersist.getData('threads');
  if (!threads) return {};

  return threads.map(function(threadId){
    return {id: threadId};
  });
};

/*
 * @method startFollowing
 * @param {function} appendPostsCallback
 * @param {function} errorCallback
 * @param {int|undefined} updateRate
 */
Thread.prototype.startFollowing = function(appendPostsCallback, errorCallback, updateRate){
  updateRate = limitUpdateRate(updateRate);
  fetchNewPosts.call(this, appendPostsCallback, errorCallback);

  this._interval = window.setInterval(function(){
    fetchNewPosts.call(this, appendPostsCallback, errorCallback);
  }.bind(this), updateRate);
};

/*
 * @method stopFollowing
 */
Thread.prototype.stopFollowing = function(){
  window.clearInterval(this._interval);
};

/*
 * @method filterOlderPosts
 * @param {array} posts
 * @param {int} timestamp
 * @returns {array}
 */
Thread.prototype.filterOlderPosts = function(posts, timestamp){
  return posts.filter(function(post){
    return timestamp === undefined || post.created > timestamp;
  });
};

/*
 * @method parseNewPosts
 * @param {json} rawData
 * @returns {array}
 */
Thread.prototype.parseNewPosts = function(rawData){
  try {
    return rawData.data.children.map(function(child){
      var data = child.data;
      return ({
        id: data.id,
        title: data.title,
        author: data.author,
        url: data.url,
        created: data.created_utc
      });
    });
  } catch (e) {
    console.error('Error parsing resonse from raw data');
  }
};

/*
 * fetchNewPosts
 * @param {function} appendPostsCallback
 * @param {function} errorCallback
 */
function fetchNewPosts(appendPostsCallback, errorCallback){
  RedditService.fetchNewPosts(this._threadId).then(function(rawData){
    var posts = this.filterOlderPosts(this.parseNewPosts(rawData), this._lastTimestamp);

    saveLastCreated.call(this, posts);

    appendPostsCallback(this._threadId, Immutable.fromJS(posts));
  }.bind(this)).catch(function(error){
    this.stopFollowing();
    errorCallback(this._threadId);
  }.bind(this));
}

/*
 * saveLastCreated
 * @param {array} posts
 */
function saveLastCreated(posts){
  var firstPost = posts[0];
  if (firstPost) {
    this._lastTimestamp = firstPost.created;
  }
}

/*
 * limitUpdateRate
 * @param {int|undefined} updateRate
 * @returns {int}
 */
function limitUpdateRate(updateRate){
  var defaultUpdateRate = 10000;

  updateRate = updateRate || defaultUpdateRate;
  if (updateRate < defaultUpdateRate) updateRate = defaultUpdateRate;

  return updateRate;
}

module.exports = Thread;
