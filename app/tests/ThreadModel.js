var expect = require('chai').expect,
  ThreadModel = require('./../models/Thread'),
  dummyData = require('./redditDummyData.js');

describe('ThreadModel', function(){
  var model;
  beforeEach(function(){
    model = new ThreadModel('AskReddit');
  });
  describe('parseNewPosts', function(){
    it('it extracts posts from response with appropriate fields', function(){
      var parsedPosts = model.parseNewPosts(dummyData.onePage);

      expect(parsedPosts.length).to.be.equal(25);
      expect(parsedPosts[0]).to.have.property('id', '3qrbrp');
      expect(parsedPosts[0]).to.have.property('title', 'I made an in-browser tool for visualizing stochastic Petri Nets, C&amp;C welcome!');
      expect(parsedPosts[0]).to.have.property('url', 'https:\/\/stochastic-mechanics.herokuapp.com\/');
      expect(parsedPosts[0]).to.have.property('author', 'LamarOdom_DoomAlarm');
      expect(parsedPosts[0]).to.have.property('created', 1446154307);
    });
  });
  describe('filterOlderPosts', function(){
    it('it filters posts older than given date considering "created" attribute', function(){
      var filteredPosts = model.filterOlderPosts([{created: 1446154000}, {created: 1446153000}], 1446153000);

      expect(filteredPosts.length).to.be.equal(1);
      expect(filteredPosts[0]).to.have.property('created', 1446154000);
    });

    it('it dont filters if timestamp argument is undefined', function(){
      var filteredPosts = model.filterOlderPosts([{created: 1446154000}, {created: 1446153000}]);

      expect(filteredPosts.length).to.be.equal(2);
      expect(filteredPosts[0]).to.have.property('created', 1446154000);
      expect(filteredPosts[1]).to.have.property('created', 1446153000);
    });
  });
});
