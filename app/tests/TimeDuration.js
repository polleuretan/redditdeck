var expect = require('chai').expect,
  TimeDuration = require('./../lib/TimeDuration');

describe('TimeDuration', function(){
  describe('format', function(){
    it('it formats time below 1 minute', function(){
      expect(TimeDuration.format(0)).to.be.equal('less than 1m');
      expect(TimeDuration.format(10)).to.be.equal('less than 1m');
      expect(TimeDuration.format(59)).to.be.equal('less than 1m');
    });
    it('it formats time below 1 hour', function(){
      var secInMinute = 60;
      expect(TimeDuration.format(secInMinute * 1)).to.be.equal('1m');
      expect(TimeDuration.format(secInMinute * 33 + 11)).to.be.equal('33m');
      expect(TimeDuration.format(secInMinute * 59 + 59)).to.be.equal('59m');
    });
    it('it formats time below 1 day', function(){
      var secInHour = 3600;
      expect(TimeDuration.format(secInHour * 1)).to.be.equal('1h');
      expect(TimeDuration.format(secInHour * 13 + 111)).to.be.equal('13h');
      expect(TimeDuration.format(secInHour * 23 + 3599)).to.be.equal('23h');
    });
    it('it formats time above 1 day', function(){
      var secInDay = 86400;
      expect(TimeDuration.format(secInDay * 1)).to.be.equal('1d');
      expect(TimeDuration.format(secInDay * 13 + 111)).to.be.equal('13d');
      expect(TimeDuration.format(secInDay * 23 + 123)).to.be.equal('23d');
    });
  });
});
