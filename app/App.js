var ReactDOM = require('react-dom'),
  React = require('react'),
  RootComponent = require('./components/Root'),
  actions = require('./actions'),
  Store = require('./Store'),
  ThreadModel = require('./models/Thread');

/*
 * @constructs App
 */
var App = function(){
  var initialData = {threads: ThreadModel.getRememberedThreads()};
  this.store = new Store(initialData);
};

/*
 * @method render
 * @param {DOM} element
 */
App.prototype.render = function(element){
  ReactDOM.render(React.createElement(RootComponent, {
    store: this.store,
    actions: this.actions
  }), element);
};

module.exports = App;
