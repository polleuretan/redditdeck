var React = require('react'),
  Immutable = require('immutable'),
  constants = require('./../constants'),
  actions = require('./../actions'),
  ThreadContainer = require('./ThreadContainer'),
  ThreadAddForm = require('./ThreadAddForm');

var THREAD_WIDTH = 400;
var THREAD_MARGIN = 5;

var Root = React.createClass({
  getInitialState: function(){
    return ({
      threads: Immutable.List()
    });
  },
  componentDidMount: function(){
    this.props.store.on(constants.Events.CHANGE, this.applyStoreState);

    this.applyStoreState();
  },
  componentWillUnmount: function(){
    this.props.store.removeListener(constants.Events.CHANGE,
      this.applyStoreState);
  },
  applyStoreState: function(){
    this.setState({
      threads: this.props.store.getThreads()
    });
  },
  calculateWidth: function(){
    var threadsCount = this.state.threads.count(),
      width = (THREAD_WIDTH * threadsCount) + THREAD_MARGIN * (threadsCount - 1);

    if (width <= 0) width = THREAD_WIDTH;

    return width;
  },
  renderThreads: function(threads){
    var store = this.props.store;
    return threads.map(function(thread, idx){
      var threadId = thread.get('id');
      return <ThreadContainer
        key={threadId} threadId={threadId} store={store}/>;
    }, this);
  },
  getThreadIds: function(threads){
    return threads.map(function(thread){
      return thread.get('id');
    });
  },
  render: function(){
    var threads = this.state.threads;
    return (
      <div className="wrapper" style={{width: this.calculateWidth()}}>
        <header>
          <ThreadAddForm onAdd={actions.addThread}
            threadIds={this.getThreadIds(threads)}/>
        </header>
        <main>{this.renderThreads(threads)}</main>
        <footer></footer>
      </div>
    );
  }
});

module.exports = Root;
