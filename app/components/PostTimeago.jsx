var React = require('react'),
  TimeDuration = require('./../lib/TimeDuration');

var TIMEAGO_UPDATE_INTERVAL = 60000;

var PostTimeago = React.createClass({
  propTypes: {
    created: React.PropTypes.number.isRequired
  },
  getInitialState: function(){
    return ({
      timeago: ''
    });
  },
  shouldComponentUpdate: function(nextProps, nextState){
    return (nextState.timeago !== this.state.timeago);
  },
  componentDidMount: function(){
    this.updateTimeAgo();

    this._interval = window.setInterval(this.updateTimeAgo,
      TIMEAGO_UPDATE_INTERVAL);
  },
  componentWillUnmount: function(){
    window.clearInterval(this._interval);
  },
  getTimeAgo: function(created){
    var now = Math.floor(Date.now()/1000);
    return TimeDuration.format(now - created);
  },
  updateTimeAgo: function(){
    var timeago = this.getTimeAgo(this.props.created);
    this.setState({
      timeago: timeago
    });
  },
  render: function(){
    return (
      <span className="post-timeago">{this.state.timeago}</span>
    );
  }
});

module.exports = PostTimeago;
