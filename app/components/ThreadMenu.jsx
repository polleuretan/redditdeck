var React = require('react'),
  Immutable = require('immutable'),
  actions = require('./../actions'),
  constants = require('./../constants');

var ThreadMenu = React.createClass({
  propTypes: {
    threadId: React.PropTypes.string.isRequired,
  },
  getInitialState: function(){
    return ({
      visible: false
    });
  },
  shouldComponentUpdate: function(nextProps, nextState){
    return nextState.visible !== this.state.visible;
  },
  setVisibility: function(visible){
    this.setState({
      visible: visible
    });
  },
  markAllViewed: function(){
    actions.allPostsViewed(this.props.threadId);
  },
  render: function(){
    return (
      <nav onMouseEnter={this.setVisibility.bind(this, true)}
        onMouseLeave={this.setVisibility.bind(this, false)}
        className="thread-menu">
        <span className="arrow">&#9656;</span>
        <ul style={{display: (this.state.visible) ? 'block' : 'none'}}
          onClick={this.setVisibility.bind(this, false)}
          className="thread-menu-list">
          <li onClick={this.markAllViewed} className="thread-menu-item">
            Mark all viewed
          </li>
        </ul>
      </nav>
    );
  }
});

module.exports = ThreadMenu;
