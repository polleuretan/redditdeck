var React = require('react'),
  Immutable = require('immutable'),
  constants = require('./../constants'),
  PostTimeago = require('./PostTimeago');

var Post = React.createClass({
  propTypes: {
    post: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    onView: React.PropTypes.func.isRequired,
  },
  shouldComponentUpdate: function(nextProps, nextState){
    return (nextProps.post !== this.props.post);
  },
  onView: function(e){
    this.props.onView(this.props.post.get('id'));
  },
  getOnView: function(){
    if (this.isNew()) {
      return this.onView;
    }
  },
  isNew: function(){
    return this.props.post.get(constants.Threads.IS_NEW_ATTRIBUTE);
  },
  renderIsNew: function(){
    if (this.isNew()) {
      return (
        <span onClick={this.onView} className="new-post-label">
          NEW!
        </span>
      );
    }
  },
  getTimeAgo: function(post){
    var now = Math.floor(Date.now()/1000);
    return TimeDuration.format(now - post.get('created'));
  },
  render: function(){
    var post = this.props.post;
    return (
      <li className="post">
        <div className="post-title-container">
          <a href={post.get('url')} target="_blank" onClick={this.getOnView()}
            className="post-title">
            {post.get('title')}
          </a>
          <p className="post-author-container">
            <span className="post-author">by {post.get('author')}</span>
          </p>
        </div>
        {this.renderIsNew()}
        <PostTimeago created={post.get('created')}/>
      </li>
    );
  }
});

module.exports = Post;
