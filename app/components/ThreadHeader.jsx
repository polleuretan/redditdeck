var React = require('react'),
  Immutable = require('immutable'),
  constants = require('./../constants');

var ThreadHeader = React.createClass({
  propTypes: {
    title: React.PropTypes.string.isRequired,
    posts: React.PropTypes.instanceOf(Immutable.List).isRequired
  },
  shouldComponentUpdate: function(nextProps, nextState){
    return (nextProps.posts !== this.props.posts);
  },
  countNewPosts: function(){
    return this.props.posts.reduce(function(sum, post){
      if (post.get(constants.Threads.IS_NEW_ATTRIBUTE)) {
        sum += 1;
      }
      return sum;
    }, 0);
  },
  renderNewPosts: function(){
    var numberOfNewPosts = this.countNewPosts();
    if (numberOfNewPosts) {
      return <span>({numberOfNewPosts})</span>;
    }
  },
  render: function(){
    return (
      <h2 className="thread-title">{this.props.title} {this.renderNewPosts()}</h2>
    );
  }
});

module.exports = ThreadHeader;
