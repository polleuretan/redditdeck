var React = require('react'),
  Immutable = require('immutable'),
  constants = require('./../constants'),
  actions = require('./../actions'),
  Store = require('./../Store'),
  ThreadModel = require('./../models/Thread'),
  Thread = require('./Thread');

var ThreadContainer = React.createClass({
  propTypes: {
    threadId: React.PropTypes.string.isRequired,
    store: React.PropTypes.instanceOf(Store).isRequired,
  },
  getInitialState: function(){
    return ({
      posts: Immutable.List(),
      error: ''
    });
  },
  applyStoreState: function(){
    var thread = this.props.store.getThread(this.props.threadId);
    this.setState({
      posts: this.props.store.getPosts(this.props.threadId),
      error: thread.get('error')
    });
  },
  componentDidMount: function(){
    this.threadModel = new ThreadModel(this.props.threadId);
    this.threadModel.startFollowing(actions.newPosts,
      actions.newPostsFail);

    this.props.store.on(constants.Events.CHANGE, this.applyStoreState);
    this.applyStoreState();
  },
  onClose: function(){
    this.threadModel.stopFollowing();
    this.props.store.removeListener(constants.Events.CHANGE,
      this.applyStoreState);

    actions.closeThread(this.props.threadId);
  },
  onPostView: function(postId){
    actions.postViewed(this.props.threadId, postId);
  },
  render: function(){
    return (
      <Thread threadId={this.props.threadId} posts={this.state.posts}
        error={this.state.error} onClose={this.onClose}
        onPostView={this.onPostView} />
    );
  }
});

module.exports = ThreadContainer;
