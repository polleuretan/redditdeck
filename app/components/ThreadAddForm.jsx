var React = require('react'),
  Immutable = require('immutable'),
  constants = require('./../constants');

var ThreadAddForm = React.createClass({
  propTypes: {
    onAdd: React.PropTypes.func.isRequired,
    threadIds: React.PropTypes.instanceOf(Immutable.List).isRequired,
  },
  shouldComponentUpdate: function(){
    return false;
  },
  onSubmit: function(e){
    e.preventDefault();
    var threadId = this.refs.threadIdInput.value;
    if (threadId && !this.props.threadIds.includes(threadId)) {
      this.props.onAdd(threadId);
      this.clearThreadIdInput();
    }
  },
  clearThreadIdInput: function(){
    this.refs.threadIdInput.value = '';
  },
  render: function(){
    return (
      <form className="thread-add" onSubmit={this.onSubmit}>
        <input ref="threadIdInput" type="text" placeholder="Example: AskReddit"/>
        <input type="submit" value="Add thread"/>
      </form>
    );
  }
});

module.exports = ThreadAddForm;
