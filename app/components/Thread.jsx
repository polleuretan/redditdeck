var React = require('react'),
  Immutable = require('immutable'),
  ThreadHeader = require('./ThreadHeader'),
  ThreadMenu = require('./ThreadMenu'),
  Post = require('./Post');

var Thread = React.createClass({
  propTypes: {
    threadId: React.PropTypes.string.isRequired,
    onClose: React.PropTypes.func.isRequired,
    onPostView: React.PropTypes.func.isRequired,
    posts: React.PropTypes.instanceOf(Immutable.List).isRequired
  },
  shouldComponentUpdate: function(nextProps, nextState){
    return (nextProps.posts !== this.props.posts ||
        nextProps.error !== this.props.error);
  },
  renderPosts: function(posts){
    return posts.map(function(post){
      return (
        <Post onView={this.props.onPostView} key={post.get('id')} post={post} />
      );
    }, this);
  },
  render: function(){
    return (
      <section className="thread">
        <ThreadHeader title={this.props.threadId} posts={this.props.posts}/>
        <ThreadMenu threadId={this.props.threadId}/>
        <button className="close-thread" onClick={this.props.onClose}>
          &times;
        </button>
        <h3 className="thread-error">{this.props.error}</h3>
        <ul className="posts-list">{this.renderPosts(this.props.posts)}</ul>
      </section>
    );
  }
});

module.exports = Thread;
