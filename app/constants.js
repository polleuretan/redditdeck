module.exports = {
  Events: {
    CHANGE: 'CHANGE_EVENT',
  },
  Threads: {
    CREATE_ACTION: 'CREATE_THREAD_ACTION',
    CLOSE_ACTION: 'CLOSE_THREAD_ACTION',
    IS_NEW_ATTRIBUTE: '__new__',
  },
  Posts: {
    NEW_POSTS_ACTION: 'NEW_POSTS_ACTION',
    NEW_POSTS_FAIL_ACTION: 'NEW_POSTS_FAIL_ACTION',
    VIEWED_ACTION: 'POST_VIEWED_ACTION',
    ALL_VIEWED_ACTION: 'ALL_POSTS_VIEWED_ACTION',
  }
};
