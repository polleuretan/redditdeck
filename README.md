# README #

### Redditdeck ###

Sample app for viewing reddit threads inspired by [TweetDeck](https://tweetdeck.twitter.com/).
Created in educational purposes.

### Setup and run ###

* git clone git@bitbucket.org:polleuretan/redditdeck.git
* run *npm install* to install dependencies
* run *npm run build* to build project files
* run *node index.js* to serve static files
* navigate your browser to http://localhost:3000/