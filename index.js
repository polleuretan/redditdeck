var Express = require('express'),
  path = require('path');

var app = Express();

app.use(Express.static(__dirname + '/public'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/public/index.html');
});

server = app.listen(process.env.PORT || 3000, function(){
  console.log('Server started on port %s...', server.address().port);
});
