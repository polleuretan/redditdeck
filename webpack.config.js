var webpack = require("webpack");
module.exports = {
  entry: {
    main: './app/main.js',
    vendors: ['react', 'react-dom', 'immutable', 'flux', 'events', 'promise']
  },
  output: {
    path: __dirname + '/public/js',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx$/,
        loader: 'jsx-loader?insertPragma=React.DOM'
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js')
  ]
};
